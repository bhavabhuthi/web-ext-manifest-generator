
  document.getElementById("submit").addEventListener("click", function(){
    // Assigning default Values for the Variables
    var extname = "Example Extension", extversion = "1.0", extdescription = "Extension Description", extpos = "";

    // Assigning user entered for the Variables
    var dummy = document.getElementById('extname').value;
     if(dummy != "") {
       extname = dummy;
    }

    var dummy = document.getElementById('extdescription').value;
     if(dummy != "") {
      extdescription = dummy;
    }

    var dummy = document.getElementById('extversion').value;
     if(dummy != "") {
       extversion = dummy;
    }

    //Creation of Object with given parameters.
    var dummy = new Object();
    var data = new Object();
    data.name = extname;
    data.description = extdescription;
    data.version = extversion;

    if(document.getElementById("page").checked) {
        dummy.default_icon = {
          "32":"path/to/example-16X16.png",
          "64":"path/to/example-32X32.png"
        };
        dummy.default_title = "Title";
        dummy.default_popup = "path/to/examplepage.html";

        data.page_action = dummy;
        console.log(data.page_action);

    } else if(document.getElementById("browser").checked) {
      dummy.default_icon = {
        "32":"path/to/example-16X16.png",
        "64":"path/to/example-32X32.png"
      };
      dummy.default_title = "Title";
      dummy.default_popup = "path/to/examplepage.html";

      data.browser_action = dummy;
    }

    var datatext = JSON.stringify(data, null, 6);

    console.log(datatext);

    var filename = "manifest.json";


    // Generate download with JSON content
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(datatext));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }, false);
